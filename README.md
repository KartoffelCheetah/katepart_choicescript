# CHOICESCRIPT syntax highlighting - KATEPART

[KatePart](https://docs.kde.org/stable5/en/applications/katepart/highlight.html) is a universal text editor component for `KDE editors`. Works with editors like **`Kate`, `KDevelop`, `KWrite`**, and probably more!


| Screenshot made in KDevelop      |
| :------------------------------: |
| ![](example.png)                 |

## Installation

### A.) Normal Installation

For both Linux and Windows put the
[choicescript.xml](./choicescript.xml) file
into your `org.kde.syntax-highlighting/syntax/` directory
(if you've never did anything like this before, you may have to create the directory first).

* On Windows it's:
`%USERPROFILE%/AppData/Local/org.kde.syntax-highlighting/syntax/`.
* On Linux it's: `~/.local/share/org.kde.syntax-highlighting/syntax/`.

### B.) (Optional) Pro Installation Method:

Clone with git and make a symbolic link to the file.

```shell
git clone https://gitlab.com/KartoffelCheetah/katepart_choicescript.git
ln -s $PWD/katepart_choicescript/choicescript.xml $HOME/.local/share/org.kde.syntax-highlighting/syntax/choicescript.xml
```
